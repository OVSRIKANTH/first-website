var gmode = document.getElementById('gmode');
var spmode = document.getElementById('spmode');
var dpmode = document.getElementById('dpmode');
var board = document.getElementById('board');
var end = document.getElementById('endgame');

var originalboard=['empty','empty','empty','empty','empty','empty','empty','empty','empty']
var wincases=[[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7,],[2,5,8],[0,4,8],[2,4,6]]

var a=-1,b=-1;

function mode(value){
    a=value;

    if(a===1 && b!==-1){
        gmode.style.display= 'none'
        spmode.style.display='block'
    }
    else if(a===2 && b!==-1)   {
        gmode.style.display= 'none'
        dpmode.style.display='block'
    }
}

document.getElementById('choicex').addEventListener('click', choice);
document.getElementById('choiceo').addEventListener('click', choice);

function choice(e){
    b=e.target.value;
    //e.target.style.backgroundColor="blue";
    if(a===1 && b!==-1){
        gmode.style.display= 'none';
        spmode.style.display='block';
    }
    else if(a===2 && b!==-1){
        gmode.style.display= 'none';
        dpmode.style.display='block';
    }
}
var x,y;
document.getElementById('spplay').addEventListener('click',spboard);
function spboard(e){
    e.preventDefault()
    x=document.getElementById('x').value;
    y='Computer';
    spmode.style.display = 'none';
    board.style.display = 'block';
    singleplayer();
}

document.getElementById('dpplay').addEventListener('click',dpboard);
function dpboard(e){
    e.preventDefault()
    x=document.getElementById('y').value;
    y=document.getElementById('z').value;
    dpmode.style.display = 'none';
    board.style.display = 'block';
}

function resetboard(){
 originalboard= ['empty','empty','empty','empty','empty','empty','empty','empty','empty'];
    for(var i=0;i<9;i++){
        document.getElementById(i).innerText='';
        document.getElementById(i).style.backgroundColor = "#3CBC8D";
    }
    
    end.style.display='none';
    singleplayer();
}
var hum,com
function singleplayer(){
    var originalboard=['empty','empty','empty','empty','empty','empty','empty','empty','empty']
    if(b===0){hum='O';com='X'}
    else{hum='X';com='O'}
    for(var i=0;i<9;i++){
        document.getElementById(i).addEventListener('click',turnClick);
    }
}

var winner;
function turnClick(e){
    if(originalboard[e.target.id] === 'empty'){
    turn(e.target.id,hum)    
    winner=checkWin(originalboard);
    turn(minimax(originalboard, com).index ,com);
    checkWin(originalboard);
    }
}


function turn(squareid,player){
    originalboard[squareid]=player;
    document.getElementById(squareid).innerText= player;
}

function checkWin1(cwboard){
    for(var i=0;i<8;i++){
        if(cwboard[wincases[i][0]]===hum && cwboard[wincases[i][1]]===hum && cwboard[wincases[i][2]]===hum){
            return hum;
        }
        else if(cwboard[wincases[i][0]]===com && cwboard[wincases[i][1]]===com && cwboard[wincases[i][2]]===com){        
            return com;
        }
        
    }
}

function checkWin(cwboard){
    for(var i=0;i<8;i++){
        if(cwboard[wincases[i][0]]===hum && cwboard[wincases[i][1]]===hum && cwboard[wincases[i][2]]===hum){
            endgame(hum,wincases[i])
            return hum;
        }
        else if(cwboard[wincases[i][0]]===com && cwboard[wincases[i][1]]===com && cwboard[wincases[i][2]]===com){
            endgame(com,wincases[i])
            return com;
        }
        
    }
    checkTie();
    
}
function checkTie(){
    var tie='Yes'
    for(var i=0;i<9;i++){
        if(originalboard[i]==='empty'){
            tie='No'
        }
    }
    if(tie==='Yes'){
        endgame('tie',[0,0,0])
    }
}

function endgame(player,cases){
    if(player===hum){
        end.innerText= x + " Won!";
        for(var i=0;i<3;i++){
            document.getElementById(cases[i]).style.backgroundColor = "red";
        }
    }
    else if(player===com){
        end.innerText= x + " lost";
        for(var i=0;i<3;i++){
            document.getElementById(cases[i]).style.backgroundColor = "red";
        }
    }
    else if(player==='tie'){
        end.innerText= "Tie Game";
        for(var i=0;i<9;i++){
            document.getElementById(i).style.backgroundColor = "blue";
        }
    }
    //board.style.display='none';
    //document.getElementsByTagName('table')[0].style.display='none';
    end.style.display='block';
    for(var i=0;i<9;i++){
        document.getElementById(i).removeEventListener('click',turnClick);
    }

}

function emptySquares(eboard) {
    var l=[];
    for(var i=0;i<9;i++){
        if(eboard[i]==='empty'){
            l.push(i);
        }
    }
    return l;
    
    
    //return eboard.filter(s => s === 'empty');
}

function minimax(newBoard, player){
    var availSpots = emptySquares(newBoard);     //newBoard.filter(s => s == 'empty');
    console.log(availSpots);

	if (checkWin1(newBoard) === hum) {
		return {score: -10};
	} else if (checkWin1(newBoard) === com) {
		return {score: 10};
	} else if (availSpots.length === 0) {
		return {score: 0};
	}
	var moves = [];
	for (var i = 0; i < availSpots.length; i++) {
		var move = {};
        //move.index = newBoard[availSpots[i]];
        move.index = availSpots[i];
		newBoard[availSpots[i]] = player;

		if (player === com) {
			var result = minimax(newBoard, hum);
			move.score = result.score;
		} else {
			var result = minimax(newBoard, com);
			move.score = result.score;
		}

		newBoard[availSpots[i]] = 'empty';
        //move.index = availSpots[i];
		moves.push(move);
	}

	var bestMove;
	if(player === com) {
		var bestScore = -10000;
		for(var i = 0; i < moves.length; i++) {
			if (moves[i].score > bestScore) {
				bestScore = moves[i].score; 
				bestMove = i;
			}
		}
	} else {
		var bestScore = 10000;
		for(var i = 0; i < moves.length; i++) {
			if (moves[i].score < bestScore) {
				bestScore = moves[i].score;
				bestMove = i;
			}
		}
	}

    return moves[bestMove];
}

























